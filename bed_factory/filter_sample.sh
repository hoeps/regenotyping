

sample=$1
column=$2
add_randomization=true

cut -d$'\t' -f1,2,8,$column genotypes_forfunction.vcf > 'temp/invs_'$sample'.txt'

grep 0$ 'temp/invs_'$sample'.txt' > 'temp/'$sample'_wt_pre.bed'
grep 1$ 'temp/invs_'$sample'.txt' > 'temp/'$sample'_het_pre.bed'
grep 2$ 'temp/invs_'$sample'.txt' > 'temp/'$sample'_hom_pre.bed'

sed -e 's/;SVLEN.*//g' 'temp/'$sample'_wt_pre.bed' | sed -e 's/SVTYPE=INV;END=//g' > 'to_test/'$sample'_wt.bed'
sed -e 's/;SVLEN.*//g' 'temp/'$sample'_het_pre.bed' | sed -e 's/SVTYPE=INV;END=//g' > 'to_test/'$sample'_het.bed'
sed -e 's/;SVLEN.*//g' 'temp/'$sample'_hom_pre.bed' | sed -e 's/SVTYPE=INV;END=//g' > 'to_test/'$sample'_hom.bed'

if [ "add_randomization" = true ] ; then
    ./randomize.sh 'temp/invs_'$sample'.txt' 'to_test/'$sample'_wt_rand.bed'
    ./randomize.sh 'temp/invs_'$sample'.txt' 'to_test/'$sample'_het_rand.bed'
    ./randomize.sh 'temp/invs_'$sample'.txt' 'to_test/'$sample'_hom_rand.bed'
fi
